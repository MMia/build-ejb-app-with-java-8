package com.library.app.author.resource;

import javax.enterprise.context.*;

import com.google.gson.*;
import com.library.app.author.model.*;
import com.library.app.common.json.*;

@ApplicationScoped
public class AuthorJSONConventer implements EntityJsonConverter<Author>{
	
	@Override
	public Author convertFrom(final String json) {
		final JsonObject jsonObject = JsonReader.readAsJsonObject(json); 
		
		final Author author = new Author();
		author.setName(JsonReader.getStringOrNull(jsonObject, "name"));
		
		return author;
	}
	
	@Override
	public JsonElement convertToJsonElement(final Author author) {
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("id", author.getId());
		jsonObject.addProperty("name", author.getName());
		return jsonObject;
	}

}
