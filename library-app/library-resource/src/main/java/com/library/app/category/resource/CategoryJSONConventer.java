package com.library.app.category.resource;

import javax.enterprise.context.*;

import com.google.gson.*;
import com.library.app.category.model.*;
import com.library.app.common.json.*;

@ApplicationScoped
public class CategoryJSONConventer implements EntityJsonConverter<Category>{
	
	@Override
	public Category convertFrom(final String json) {
		final JsonObject jsonObject = JsonReader.readAsJsonObject(json); 
		
		final Category category = new Category();
		category.setName(JsonReader.getStringOrNull(jsonObject, "name"));
		
		return category;
	}
	
	@Override
	public JsonElement convertToJsonElement(final Category category) {
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("id", category.getId());
		jsonObject.addProperty("name", category.getName());
		
		return jsonObject;
	}

}
